# @cloudweight/web-app

## Configuration

### dotenv

To configure local development environment create `.env` file with following contents:
```
SENTRY_ENV=local
DIETITIAN_API_URL=http://localhost:4000
AUTH_DOMAIN=dietetykwchmurze-stage.eu.auth0.com
AUTH_CLIENT_ID=4lNaLyV8XXcyUcXI4yAyZ5bFtgOzQ7Wn
AUTH_AUDIENCE=api.stage.dietetykwchmurze.pl
```
