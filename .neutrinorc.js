require('dotenv').config()
const path = require('path')
const standard = require('@neutrinojs/standardjs')
const react = require('@neutrinojs/react')
const jest = require('@neutrinojs/jest')
const SentryWebpackPlugin = require('@sentry/webpack-plugin')

const moduleResolverConfig = {
  alias: {
    '^~/(.+)': './src/\\1'
  },
  extensions: ['', '.js', '.jsx', '.ts', '.tsx']
}

module.exports = {
  options: {
    root: __dirname,
    extensions: ['mjs', 'jsx', 'js', 'tsx', 'ts'],
    tests: '__tests__',
  },
  use: [
    standard({
      eslint: {
        baseConfig: {
          extends: [
            'plugin:react/recommended',
            'plugin:import/errors',
            'plugin:import/recommended',
            'plugin:import/typescript',
            'plugin:@typescript-eslint/recommended',
          ],
          parser: '@typescript-eslint/parser',
          parserOptions: {
            project: path.resolve(__dirname, './tsconfig.json')
          },
          rules: {
            'import/no-internal-modules': ['error', {
              allow: ['date-fns/locale/*']
            }],
            'import/max-dependencies': ['error', { 'max': 12 }],
            'import/first': 'error',
            'import/named': 'off',
            'import/no-cycle': 'error',
            'import/order': [
              'error',
              {
                pathGroups: [
                  {
                    pattern: '~/**',
                    group: 'external',
                    position: 'after'
                  }
                ]
              }
            ],
            'react/prop-types': 'off',
            '@typescript-eslint/explicit-function-return-type': ['error', {
              allowExpressions: true,
              allowTypedFunctionExpressions: true,
              allowHigherOrderFunctions: true,
            }],
            '@typescript-eslint/consistent-type-definitions': ['error', 'type'],
          },
          overrides: [
            {
              files: ['*.js'],
              rules: {
                '@typescript-eslint/no-var-requires': 'off',
              }
            },
            {
              files: ['*.spec.ts'],
              rules: {
                '@typescript-eslint/explicit-function-return-type': 'off',
                '@typescript-eslint/no-explicit-any': 'off',
              }
            }
          ],
          settings: {
            'import/resolver': {
              'babel-module': moduleResolverConfig
            },
          }
        },
      },
    }),
    react({
      html: {
        title: 'Dietetyk w Chmurze',
      },
      env: {
        SENTRY_DSN: undefined,
        SENTRY_ENV: undefined,
        AUTH_DOMAIN: undefined,
        AUTH_CLIENT_ID: undefined,
        AUTH_AUDIENCE: undefined,
        DIETITIAN_API_URL: undefined,
      },
      babel: {
        presets: [
          '@babel/typescript',
        ],
        plugins: [
          [
            'babel-plugin-module-resolver',
            moduleResolverConfig,
          ],
        ],
      },
      style: {
        loaders: [
          'postcss-loader',
        ]
      },
      devServer: {
        port: process.env.PORT || 3000,
      },
    }),
    jest({
      setupFilesAfterEnv: [
        '<rootDir>/src/tests/setup.ts'
      ]
    }),
    sentry(),
  ],
}

function sentry() {
  return (neutrino) => {
    if (!process.env.SENTRY_ENV || process.env.SENTRY_ENV === 'local') {
      return;
    }

    neutrino.config
      .plugin('sentry')
      .use(
        SentryWebpackPlugin,
        [
          {
            include: ['build', 'src'],
          }
        ]
      )
  }
}
