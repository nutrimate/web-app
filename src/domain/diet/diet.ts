import { isSameWeek, Locale } from 'date-fns'
import { MealType } from './mealType'
import { MealPlan } from './mealPlan'

export type WeeklyDiet = {
  id: string;
  userId: string;
  weekStartDay: Date;
  mealSchedule: MealType[];
  targetCalories: number;
  mealPlan: MealPlan;
}

export const isCurrentDiet = (diet: WeeklyDiet, locale: Locale, now: Date = new Date()): boolean => {
  const weekStartsOn = locale.options?.weekStartsOn

  return isSameWeek(diet.weekStartDay, now, { weekStartsOn })
}
