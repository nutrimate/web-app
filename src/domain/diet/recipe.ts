import { MealType } from './mealType'
import { Ingredient } from './ingredient'

export type Recipe = {
  id: string;
  name: string;
  imageUrl: string;
  timeToPrepare: number;
  mealTypes: MealType[];
  instructions: { step: number; description: string }[];
  ingredients: { ingredient: Ingredient; amount: number }[];
}

export type NutritionFacts = {
  calories: number;
  proteins: number;
  fats: number;
  carbohydrates: number;
}

const getRecipeIngredientNutritionFacts = (recipeIngredient: Recipe['ingredients'][number]): NutritionFacts => {
  const { ingredient, amount: recipeAmount } = recipeIngredient
  const factor = recipeAmount / ingredient.unitAmount

  return {
    calories: factor * ingredient.calories,
    proteins: factor * ingredient.proteins,
    fats: factor * ingredient.fats,
    carbohydrates: factor * ingredient.carbohydrates
  }
}

export const getRecipeNutritionFacts = (recipe: { ingredients: Recipe['ingredients'] }): NutritionFacts => {
  return recipe.ingredients
    .reduce(
      (nutritionFacts, recipeIngredient) => {
        const recipeIngredientNutritionFacts = getRecipeIngredientNutritionFacts(recipeIngredient)

        return {
          calories: nutritionFacts.calories + recipeIngredientNutritionFacts.calories,
          proteins: nutritionFacts.proteins + recipeIngredientNutritionFacts.proteins,
          fats: nutritionFacts.fats + recipeIngredientNutritionFacts.fats,
          carbohydrates: nutritionFacts.carbohydrates + recipeIngredientNutritionFacts.carbohydrates
        }
      },
      {
        calories: 0,
        proteins: 0,
        fats: 0,
        carbohydrates: 0
      }
    )
}
