import { MealType } from './mealType'
import { Recipe } from './recipe'

export type MealPlanEntry = {
  day: number;
  mealNumber: number;
  meal: MealType;
  recipe: Recipe;
};

export type MealPlan = MealPlanEntry[];

export type DailyMealPlan = { [mealNumber: number]: MealPlanEntry }
export type OrderedMealPlan = { [day: number]: DailyMealPlan }

export const orderMealPlan = (mealPlan: MealPlan): OrderedMealPlan =>
  mealPlan.reduce(
    (orderedMealPlan, entry) => {
      const { day, mealNumber } = entry

      const dayEntry = {
        ...orderedMealPlan[day],
        [mealNumber]: entry
      }

      return { ...orderedMealPlan, [day]: dayEntry }
    },
    {} as OrderedMealPlan
  )
