import { Unit } from './unit'

export type Ingredient = {
  id: string;
  name: string;
  unit: Unit;
  unitAmount: number;
  calories: number;
  proteins: number;
  fats: number;
  carbohydrates: number;
}
