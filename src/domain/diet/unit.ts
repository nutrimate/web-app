export type Unit = {
    id: string;
    name: string;
    plurals: { [key: string]: string };
}
