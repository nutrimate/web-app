import { Locale } from 'date-fns'
import plDateFnsLocale from 'date-fns/locale/pl'

export const useDateFnsLocale = (): Locale => {
  return plDateFnsLocale
}
