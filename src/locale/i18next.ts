import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import * as pl from './pl'

const resources = {
  pl
}

export const init = (language = 'pl'): void => {
  i18n
    .use(initReactI18next)
    .init({
      resources,
      lng: language,
      ns: ['diets', 'units'],
      defaultNS: '',
      interpolation: {
        escapeValue: false
      }
    })
}
