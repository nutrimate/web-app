export const config = {
  auth: {
    clientId: process.env.AUTH_CLIENT_ID,
    domain: process.env.AUTH_DOMAIN,
    audience: process.env.AUTH_AUDIENCE
  },
  sentry: {
    dsn: process.env.SENTRY_DSN,
    env: process.env.SENTRY_ENV
  },
  dietitianApiUrl: process.env.DIETITIAN_API_URL
}
