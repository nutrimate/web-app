import { init } from '@sentry/browser'
import { config } from './config'

if (process.env.SENTRY_ENV !== 'local') {
  init({
    dsn: config.sentry.dsn,
    environment: config.sentry.env
  })
}
