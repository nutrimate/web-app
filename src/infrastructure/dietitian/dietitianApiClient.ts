import { parseISO } from 'date-fns'
import { config } from '~/config'
import { WeeklyDiet } from '~/domain'
import { AccessTokenProvider, createHttpClient, HttpClient } from '../shared'

type WeeklyDietDto = Omit<WeeklyDiet, 'weekStartDay'> & { weekStartDay: string }

export class DietitianApiClient {
  private readonly httpClient: HttpClient

  constructor (accessTokenProvider: AccessTokenProvider) {
    this.httpClient = createHttpClient({
      baseURL: config.dietitianApiUrl,
      accessTokenProvider
    })
  }

  getDiets (): Promise<WeeklyDiet[]> {
    return this.httpClient.get<WeeklyDietDto[]>('/user/diets')
      .then(r => r.data.map(diet => ({
        ...diet,
        weekStartDay: parseISO(diet.weekStartDay)
      })))
  }
}
