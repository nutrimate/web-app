import React, { FC, createContext, useContext } from 'react'
import { DietitianApiClient } from './dietitianApiClient'

const DietitianApiClientContext = createContext<DietitianApiClient | null>(null)

export const ProvideDietitianApiClient: FC<{ dietitianApiClient: DietitianApiClient }> = props => {
  const { dietitianApiClient, children } = props

  return (
    <DietitianApiClientContext.Provider value={dietitianApiClient}>
      {children}
    </DietitianApiClientContext.Provider>
  )
}

export const useDietitianApiClient = (): DietitianApiClient => {
  const dietitianApiClient = useContext(DietitianApiClientContext)

  if (dietitianApiClient == null) {
    throw new Error('Tried to use useMealPlannerApiClient hook without initializing MealPlannerApiClient')
  }

  return dietitianApiClient
}
