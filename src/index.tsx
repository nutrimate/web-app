import './sentry'
import 'react-hot-loader'
import React, { Suspense } from 'react'
import { render } from 'react-dom'
import { Auth0Provider } from '@auth0/auth0-react'
import { CssBaseline } from '@material-ui/core'
import { config } from './config'
import { App } from './pages'
import './index.css'

render(
  (
    <>
      <CssBaseline />
      <Auth0Provider
        domain={config.auth.domain || ''}
        clientId={config.auth.clientId || ''}
        redirectUri={window.location.origin}
        audience={config.auth.audience}
      >
        <Suspense fallback={null}>
          <App />
        </Suspense>
      </Auth0Provider>
    </>
  ),
  document.getElementById('root')
)
