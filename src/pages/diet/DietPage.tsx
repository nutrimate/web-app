import React, { FC, useCallback } from 'react'
import { CircularProgress, Box } from '@material-ui/core'
import { Switch, Route, Redirect, RouteComponentProps } from 'react-router-dom'
import { useDiets } from './useDiets'
import { DietsList } from './DietsList'
import { routes } from './shared'
import { MealPlanEntryView } from './MealPlanEntryView'

export const DietPage: FC = () => {
  const { diets, isLoading } = useDiets()

  const renderMealPlanEntry = useCallback(
    (props: RouteComponentProps<{ dietId: string; day: string; mealNumber: string }>) => {
      const { dietId, day, mealNumber } = props.match.params
      const diet = diets.find(d => d.id === dietId)
      const mealPlanEntry = diet &&
        diet.mealPlan
          .find(e => e.day === Number(day) && e.mealNumber === Number(mealNumber))

      if (!mealPlanEntry) {
        return <Redirect to={routes.allDiets()} />
      }

      return (
        <MealPlanEntryView entry={mealPlanEntry} />
      )
    },
    [diets]
  )

  const renderDietsList = useCallback(
    () => <DietsList diets={diets} />,
    [diets]
  )

  return (
    <>
      {
        isLoading
          ? (
            <Box display='flex' justifyContent='center' marginTop='5em'>
              <CircularProgress />
            </Box>
          )
          : (
            <Switch>
              <Route
                exact
                path={routes.meal(':dietId', ':day', ':mealNumber')}
                render={renderMealPlanEntry}
              />
              <Route
                exact
                path={routes.allDiets()}
                render={renderDietsList}
              />
              <Route render={() => <Redirect to={routes.allDiets()} />} />
            </Switch>
          )
      }
    </>
  )
}
