import React, { FC } from 'react'
import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import { useHistory } from 'react-router-dom'
import './diet-page-app-bar.css'
import { useTranslation } from 'react-i18next'
import { routes } from '../routes'

export enum DietPageAppBarView {
  AllDiets = 'all-diets',
  MealPlanEntry = 'meal-plan-entry'
}

export type DietPageAppBarProps = {
  view?: DietPageAppBarView;
}

export const DietPageAppBar: FC<DietPageAppBarProps> = ({ view = DietPageAppBarView.AllDiets }) => {
  const history = useHistory()
  const { t } = useTranslation('diets')

  const title = view === DietPageAppBarView.MealPlanEntry ? 'mealPlanEntryView.title' : 'dietsList.title'
  const showBackButton = view === DietPageAppBarView.MealPlanEntry

  const navigateToAllDiets = (): void => history.push(routes.allDiets())

  return (
    <AppBar position='sticky' className='diet-page-app-bar'>
      <Toolbar>
        {
          showBackButton && (
            <IconButton className='diet-page-toolbar__back' edge='start' aria-label='back' onClick={navigateToAllDiets}>
              <ArrowBack />
            </IconButton>
          )
        }
        <Typography variant='h6' align='center' className='diet-page-toolbar__title'>
          {t(title)}
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
