const base = '/diets'

export const routes = {
  basePath: base,
  allDiets: () => base,
  meal: (dietId: string, day: string, mealNumber: string) => `${base}/${dietId}/day/${day}/meal/${mealNumber}`
}
