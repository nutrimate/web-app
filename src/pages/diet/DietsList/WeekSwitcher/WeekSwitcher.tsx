import React, { FC } from 'react'
import { IconButton, Typography } from '@material-ui/core'
import { ArrowLeft, ArrowRight } from '@material-ui/icons'
import { endOfWeek, format, isSameMonth } from 'date-fns'
import { useDateFnsLocale } from '~/locale'
import './week-switcher.css'

export type WeekSwitcherProps = {
  currentWeek: Date;
  disableNext: boolean;
  disablePrevious: boolean;
  onNextSelected: () => void;
  onPreviousSelected: () => void;
}

export const WeekSwitcher: FC<WeekSwitcherProps> = (props) => {
  const dateFnsLocale = useDateFnsLocale()
  const { currentWeek, disableNext, disablePrevious, onNextSelected, onPreviousSelected } = props
  const endOfWeekDate = endOfWeek(currentWeek, { weekStartsOn: 1 })

  const weekStartFormat = isSameMonth(currentWeek, endOfWeekDate) ? 'd' : 'd MMM'
  const weekStartText = format(currentWeek, weekStartFormat, { locale: dateFnsLocale })
  const weekEndText = format(endOfWeekDate, 'd MMM', { locale: dateFnsLocale })
  return (
    <div className='week-switcher'>
      <IconButton
        size='small'
        className={disablePrevious ? 'week-switcher__button--hidden' : ''}
        aria-label='previous'
        onClick={onPreviousSelected}
      >
        <ArrowLeft />
      </IconButton>
      <Typography variant='h6' className='week-switcher__text'>
        {weekStartText} - {weekEndText}
      </Typography>
      <IconButton
        size='small'
        className={disableNext ? 'week-switcher__button--hidden' : ''}
        aria-label='next'
        onClick={onNextSelected}
      >
        <ArrowRight />
      </IconButton>
    </div>
  )
}
