import React, { FC } from 'react'
import { Typography } from '@material-ui/core'
import './no-diets.css'
import { useTranslation } from 'react-i18next'

export const NoDiets: FC = () => {
  const { t } = useTranslation('diets')

  return (
    <Typography variant='subtitle1' className='no-diets'>
      {t('dietsList.noDiets')}
    </Typography>
  )
}
