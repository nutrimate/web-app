import { useMemo, useState } from 'react'
import { isCurrentDiet, WeeklyDiet } from '~/domain'
import { useDateFnsLocale } from '~/locale'

export type DietsListController = {
  currentDiet: WeeklyDiet;
  hasPreviousDiet: boolean;
  hasNextDiet: boolean;
  selectPreviousDiet: () => void;
  selectNextDiet: () => void;
}

export const useDietsListController = (diets: WeeklyDiet[]): DietsListController => {
  const dateFnsLocale = useDateFnsLocale()
  const initialDietIndex = useMemo(
    () => {
      const currentDietIndex = diets.findIndex(d => isCurrentDiet(d, dateFnsLocale))

      return currentDietIndex === -1 ? 0 : currentDietIndex
    },
    [diets, dateFnsLocale]
  )
  const [dietIndex, setDietIndex] = useState(initialDietIndex)

  const hasPreviousDiet = dietIndex > 0
  const hasNextDiet = dietIndex < diets.length - 1

  const selectPreviousDiet = (): void => hasPreviousDiet ? setDietIndex(dietIndex - 1) : undefined
  const selectNextDiet = (): void => hasNextDiet ? setDietIndex(dietIndex + 1) : undefined

  return {
    currentDiet: diets[dietIndex],
    hasPreviousDiet,
    hasNextDiet,
    selectPreviousDiet,
    selectNextDiet
  }
}
