import React, { FC } from 'react'
import { WeeklyDiet } from '~/domain'
import { DietPageAppBar } from '../shared'
import { NoDiets } from './NoDiets'
import { WeeklyDietView } from './WeeklyDietView'
import { WeekSwitcher } from './WeekSwitcher'
import { useDietsListController } from './useDietsListController'

export type DietsListProps = {
  diets: WeeklyDiet[];
}

export const DietsList: FC<DietsListProps> = ({ diets }) => {
  const { currentDiet, hasNextDiet, hasPreviousDiet, selectNextDiet, selectPreviousDiet } = useDietsListController(diets)

  if (diets.length === 0) {
    return (
      <NoDiets />
    )
  }

  const { weekStartDay } = currentDiet

  return (
    <>
      <DietPageAppBar />
      <div>
        <WeekSwitcher
          currentWeek={weekStartDay}
          disableNext={!hasNextDiet}
          disablePrevious={!hasPreviousDiet}
          onNextSelected={selectNextDiet}
          onPreviousSelected={selectPreviousDiet}
        />
        <WeeklyDietView diet={currentDiet} />
      </div>
    </>
  )
}
