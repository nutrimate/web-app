import React, { FC } from 'react'
import { ListSubheader } from '@material-ui/core'
import { addDays, format } from 'date-fns'
import { DailyMealPlan } from '~/domain'
import { useDateFnsLocale } from '~/locale'
import { DailyMealPlanListItem } from './DailyMealPlanListItem'
import './daily-meal-plan-view.css'

export type DailyMealPlanViewProps = {
  dietId: string;
  dayNumber: number;
  dailyMealPlan: DailyMealPlan;
  weekStartDay: Date;
}

export const DailyMealPlanView: FC<DailyMealPlanViewProps> = (props) => {
  const dateFnsLocale = useDateFnsLocale()
  const { dietId, dayNumber, dailyMealPlan, weekStartDay } = props
  const day = addDays(weekStartDay, dayNumber - 1)
  const dayName = format(day, 'cccc', { locale: dateFnsLocale })

  return (
    <>
      <ListSubheader disableSticky className='daily-meal-plan-view__header'>
        {dayName}
      </ListSubheader>
      {
        Object.entries(dailyMealPlan).map(([mealNumber, mealPlanEntry]) => {
          return (
            <DailyMealPlanListItem key={mealNumber} dietId={dietId} entry={mealPlanEntry} />
          )
        })
      }
    </>
  )
}
