import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { ListItem, ListItemText } from '@material-ui/core'
import classNames from 'classnames'
import { MealPlanEntry } from '~/domain'
import { routes } from '../../../../shared'
import './daily-meal-plan-list-item.css'

export type DailyMealPlanListItemProps = {
  dietId: string;
  entry: MealPlanEntry;
}

export const DailyMealPlanListItem: FC<DailyMealPlanListItemProps> = ({ dietId, entry }) => {
  const history = useHistory()
  const { day, mealNumber, meal, recipe } = entry
  const textClasses = classNames('daily-meal-plan-list-item', { 'daily-meal-plan-list-item__text--no-image': !recipe.imageUrl })
  const navigateToEntry = (): void => {
    const url = routes.meal(dietId, day.toString(), mealNumber.toString())
    history.push(url)
  }

  return (
    <ListItem button key={mealNumber} className='daily-meal-plan-list-item' onClick={navigateToEntry}>
      {recipe.imageUrl && (
        <img className='daily-meal-plan-list-item__image' src={recipe.imageUrl} alt={recipe.name} />
      )}
      <ListItemText className={textClasses} primary={recipe.name} secondary={meal.name} />
    </ListItem>
  )
}
