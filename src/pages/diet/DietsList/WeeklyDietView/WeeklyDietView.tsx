import React, { FC } from 'react'
import { List } from '@material-ui/core'
import { orderMealPlan, WeeklyDiet } from '~/domain'
import { DailyMealPlanView } from './DailyMealPlanView'

export type WeeklyDietViewProps = {
  diet: WeeklyDiet;
}

export const WeeklyDietView: FC<WeeklyDietViewProps> = ({ diet }) => {
  const { mealPlan, weekStartDay } = diet
  const orderedMealPlan = orderMealPlan(mealPlan)

  return (
    <List>
      {
        Object.entries(orderedMealPlan).map(([day, dailyMealPlan]) => {
          return (
            <DailyMealPlanView
              key={day}
              dietId={diet.id}
              weekStartDay={weekStartDay}
              dayNumber={Number(day)}
              dailyMealPlan={dailyMealPlan}
            />
          )
        })
      }
    </List>
  )
}
