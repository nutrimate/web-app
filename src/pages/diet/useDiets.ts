import useSWR from 'swr'
import { useMemo } from 'react'
import { useDietitianApiClient } from '~/infrastructure'
import { WeeklyDiet } from '~/domain'

export type DietsController = {
  diets: WeeklyDiet[];
  isLoading: boolean;
}

export const useDiets = (): DietsController => {
  const apiClient = useDietitianApiClient()
  const { data, error } = useSWR('diets', () => apiClient.getDiets())

  const sortedDiets = useMemo(
    () => {
      return (data || []).sort((a, b) => a.weekStartDay.valueOf() - b.weekStartDay.valueOf())
    },
    [data]
  )

  if (error) {
    throw error // TODO: Error handling
  }

  return {
    diets: sortedDiets,
    isLoading: data === undefined
  }
}
