import React, { FC, Fragment } from 'react'
import { Typography, Divider, ListItem, ListItemText, ListItemAvatar, Avatar, List } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { Recipe } from '~/domain'

export type RecipeInstructionsProps = {
  instructions: Recipe['instructions'];
}

export const RecipeInstructions: FC<RecipeInstructionsProps> = ({ instructions }) => {
  const { t } = useTranslation('diets')

  return (
    <div>
      <Typography variant='h5'>
        {t('mealPlanEntryView.instructions')}
      </Typography>
      <List>
        {
          instructions.map(i => {
            return (
              <Fragment key={i.step}>
                <ListItem alignItems='flex-start'>
                  <ListItemAvatar>
                    <Avatar>
                      {i.step}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={i.description}
                  />
                </ListItem>
                <Divider variant='inset' component='li' />
              </Fragment>
            )
          })
        }
      </List>
    </div>
  )
}
