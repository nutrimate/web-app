import React, { FC } from 'react'
import { Typography } from '@material-ui/core'
import { getRecipeNutritionFacts, MealPlanEntry } from '~/domain'
import { DietPageAppBar, DietPageAppBarView } from '../shared'
import { RecipeIngredients } from './RecipeIngredients'
import { RecipeInstructions } from './RecipeInstructions'
import './meal-plan-entry-view.css'

export type MealPlanEntryProps = {
  entry: MealPlanEntry;
}

export const MealPlanEntryView: FC<MealPlanEntryProps> = ({ entry }) => {
  const { recipe } = entry
  const nutritionFacts = getRecipeNutritionFacts(recipe)

  return (
    <>
      <DietPageAppBar view={DietPageAppBarView.MealPlanEntry} />
      <div className='meal-plan-entry-view'>
        {
          recipe.imageUrl && (
            <img className='meal-plan-entry-view__image' src={recipe.imageUrl} alt={recipe.name} />
          )
        }
        <Typography className='meal-plan-entry-view__title' variant='h4' align='center'>
          {recipe.name}
        </Typography>
        <Typography variant='caption' align='center'>
          {Math.round(nutritionFacts.calories)} kcal · {recipe.timeToPrepare} min
        </Typography>
        <RecipeIngredients ingredients={recipe.ingredients} />
        <RecipeInstructions instructions={recipe.instructions} />
      </div>
    </>
  )
}
