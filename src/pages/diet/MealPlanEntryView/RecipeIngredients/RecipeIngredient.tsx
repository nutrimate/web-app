import React, { FC, Fragment, useEffect, useState } from 'react'
import { Divider, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { Ingredient } from '~/domain'

export type RecipeIngredientProps = {
  ingredient: Ingredient;
  amount: number;
}

export const RecipeIngredient: FC<RecipeIngredientProps> = ({ ingredient, amount }) => {
  const { unit } = ingredient
  const { t, i18n } = useTranslation('units')
  const [isTranslationLoaded, setIsTranslationLoaded] = useState(false)

  const unitTranslationKey = `unit-${unit.id}`
  useEffect(
    () => {
      const unitNames = Object.entries(unit.plurals)
        .reduce((translations, [count, translation]) => ({ ...translations, [`${unitTranslationKey}_${count}`]: translation }), {})

      i18n.addResources(i18n.language, 'units', unitNames)
      setIsTranslationLoaded(true)
    },
    [i18n, unit, unitTranslationKey]
  )

  const unitNameAndValue = isTranslationLoaded ? `${amount} ${t(unitTranslationKey, { count: amount })}` : ''

  return (
    <Fragment key={ingredient.id}>
      <ListItem>
        <ListItemText primary={ingredient.name} />
        <ListItemSecondaryAction>
          {unitNameAndValue}
        </ListItemSecondaryAction>
      </ListItem>
      <Divider variant='inset' component='li' />
    </Fragment>
  )
}
