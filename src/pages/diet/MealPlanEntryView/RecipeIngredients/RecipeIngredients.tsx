import React, { FC } from 'react'
import { List, Typography } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { Recipe } from '~/domain'
import { RecipeIngredient } from './RecipeIngredient'

export type RecipeIngredientsProps = {
  ingredients: Recipe['ingredients'];
}

export const RecipeIngredients: FC<RecipeIngredientsProps> = ({ ingredients }) => {
  const { t } = useTranslation('diets')

  return (
    <div>
      <Typography variant='h5'>
        {t('mealPlanEntryView.ingredients')}
      </Typography>
      <List>
        {
          ingredients.map(({ ingredient, amount }) => {
            return (
              <RecipeIngredient key={ingredient.id} ingredient={ingredient} amount={amount} />
            )
          })
        }
      </List>
    </div>
  )
}
