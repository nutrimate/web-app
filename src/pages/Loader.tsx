import React, { FC } from 'react'
import { Box, CircularProgress } from '@material-ui/core'

export const Loader: FC = () => {
  return (
    <Box display='flex' justifyContent='center' alignItems='center' marginTop='15em'>
      <CircularProgress />
    </Box>
  )
}
