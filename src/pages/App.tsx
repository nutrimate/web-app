import React, { FC } from 'react'
import { useAuth0 } from '@auth0/auth0-react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { captureException } from '@sentry/browser'
import { init as initLocale } from '~/locale'
import { DietPage, routes as dietRoutes } from './diet'
import { SignInPage } from './sign-in'
import { Loader } from './Loader'

export const App: FC = () => {
  const { isAuthenticated, isLoading, error } = useAuth0()

  initLocale()

  if (isLoading) {
    return (
      <Loader />
    )
  }

  if (error) {
    captureException(error)
  }

  if (!isAuthenticated) {
    return (
      <SignInPage />
    )
  }

  return (
    <Switch>
      <Route path={dietRoutes.basePath} component={DietPage} />
      <Route render={() => <Redirect to={dietRoutes.allDiets()} />} />
    </Switch>
  )
}
