import React, { ComponentType, FC, useMemo } from 'react'
import { StylesProvider } from '@material-ui/core'
import { useAuth0 } from '@auth0/auth0-react'
import { BrowserRouter } from 'react-router-dom'
import { DietitianApiClient, ProvideDietitianApiClient } from '~/infrastructure'

const DietitianApiClientProvider: FC = ({ children }) => {
  const { getAccessTokenSilently } = useAuth0()
  const apiClient = useMemo(() => new DietitianApiClient(getAccessTokenSilently), [getAccessTokenSilently])

  return (
    <ProvideDietitianApiClient dietitianApiClient={apiClient}>
      {children}
    </ProvideDietitianApiClient>
  )
}

const dependencyProviders: FC[] = [
  ({ children }) => <StylesProvider injectFirst>{children}</StylesProvider>,
  DietitianApiClientProvider,
  ({ children }) => <BrowserRouter>{children}</BrowserRouter>
]

export function withDependencies <P> (Component: ComponentType<P>): ComponentType<P> {
  return (props: P) => {
    return dependencyProviders.reduce(
      (children, DependencyProvider) => {
        return (
          <DependencyProvider>
            {children}
          </DependencyProvider>
        )
      },
      (<Component {...props} />)
    )
  }
}
