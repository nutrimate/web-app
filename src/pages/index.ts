import { App } from './App'
import { withDependencies } from './dependencies'

const BootstrappedApp = withDependencies(App)

export { BootstrappedApp as App }
